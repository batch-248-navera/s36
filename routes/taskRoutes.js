
const express = require("express");

const taskController = require("../controllers/taskControllers");

const router = express.Router();


router.get("/",(req,res)=>{

    taskController.getAllTasks().then(result => res.send(result));
})

router.post("/",(req,res)=>{

    taskController.createTask(req.body).then(result => res.send(result));
})

router.delete("/:id",(req,res)=>{

     taskController.deleteTask(req.params.id).then(result => res.send(result));
})


router.put("/:id",(req,res)=>{

     taskController.updateTask(req.params.id,req.body).then(result => res.send(result));
})

// Activity

router.get("/:id",(req,res)=>{

    taskController.getSpecificTask(req.params.id).then(result => res.send(result));
})

router.put("/:id/:status",(req,res)=>{
    
    taskController.changeTaskStatus(req.params.id,req.params.status).then(result =>res.send(result));
    
})




module.exports = router;