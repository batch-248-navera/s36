const Task = require("../models/Task");



module.exports.getAllTasks = () =>{

    return Task.find({}).then(result=> result)

}

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		
		name : requestBody.name

	})

	return newTask.save().then((task, error) => {

		
		if (error) {

			console.log(error);
		
			return false;

		} else {

			return task; 

		}

	})

}


module.exports.deleteTask = (taskId) =>{

	return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{

		if(err){

			console.log(err);
			return false

		}else{
			return `${removedTask.name} has been deleted`;
		}
	})
}


module.exports.updateTask = (taskId, newtask) =>{

    return Task.findById(taskId).then((result,error)=>{

        if(error){
            console.log(err);
            return false;
        }
        
        result.name = newTask.name;

        return result.save().then((updatedTask,saveErr)=>{

            
            if(saveErr){
                console.log(saveErr);
                return false;
            }

            else{

                return updatedTask;

            }
        })
        
    })
}

//Activity

module.exports.getSpecificTask = (taskId) =>{

        return Task.findById(taskId).then(result => result);
}

module.exports.changeTaskStatus = (taskId,status)=>{

        return Task.findById(taskId).then((result,error)=>{

            if(error){
                console.log(err);
                return false;
            }
            
            result.status = status;

            return result.save().then((updatedStatus,saveErr)=>{

                
                if(saveErr){
                    console.log(saveErr);
                    return false;
                }

                else{

                    return updatedStatus;

                }
            })
            
        })
}