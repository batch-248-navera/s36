//Setup the dependencies
const express = require('express');
const mongoose = require('mongoose');

const taskRoutes = require('./routes/taskRoutes');

//Server setup
const app = express();
const port = 4000;
const hostname = 'localhost';

app.use(express.json());
app.use(express.urlencoded({extendedde:true}))

//Database Connection
//Connecting to MongoDB Atlas

mongoose.set('strictQuery',true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.y70nxyz.mongodb.net/s35?retryWrites=true&w=majority",{

    useNewUrlParser:true,
    useUnifiedTopology: true
});

//Connection to DB
let db = mongoose.connection;

db.on("error",console.error.bind(console,"Connction error"));
db.once("open",()=>console.log("We're connected to MongoDB Atlas!"));


app.use("/tasks", taskRoutes);

//
app.listen(port,hostname, ()=>console.log(`Server is running at http://${hostname}:${port}`))